mkdir Es3/
touch Es3/myWork.py Es3/otherStuff.py
echo '
def f(x):
    return x * x

def g(x):
    return x + 2

def h(x):
    return f(g(x))

print(h(2))
    ' > Es3/myWork.py
echo 'print("faccio tutto dopo pranzo...")' > Es3/otherStuff.py

git add Es3/myWork.py
git commit -m "Create e definite le funzioni f,g e h"
git add Es3/otherStuff.py

echo -e "\nGit 4 dummies\nTerzo esercizio \n"
echo -e "Hai appena finito la tua mattinata di lavoro e hai committato le tue modifiche. Infatti, il tuo ultimo commit è:\n\n"
git log -1
echo -e "\n\nHai scritto un piccolissimo script python in Es3/myWork.py. Noto anche che hai cominciato a lavorare a Es3/otherStuff.py, ma giustamente non lo hai committato... \nAspetta, ti sei dimenticato di mettere un .gitignore per Python!\n Non preoccuparti, fallo pure appena subito dopo pranzo :) Però attento a non committare cose a metà o indesiderate.\nPuoi pure prenderti una pausa, chiudo io l'ufficio a chiave...\n"

echo "Sei un programmatore pessimo. Il Project manager non sa nemmeno cosa sia un ciclo FOR e adesso committo tutto. Il mio nome è Pinco Hakerino, e ora lo è anche per te." > readme.MD
git config --global user.name "Pinco Hakerino"
git config --global user.email "KitiHaKera@support.ops"
git add readme.MD
git commit -m "Installate windows." readme.MD

echo -e "\n--- EMERGENZA ---\nMentre eri in pausa pranzo qualcuno ha utilizzato il tuo computer!\nSecondo il nostro sistema di sicurezza giapponese KitiHaKera, l'intruso ha lasciato qualche traccia nel readme.MD: controllalo subito!"
echo -e "\nRiporta la repository nello stato pre pausa pranzo.\nHint: Meglio se utilizzi status prima di agire, Es3/otherStuff.py è nella staging area."

