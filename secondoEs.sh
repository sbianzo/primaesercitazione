mkdir Es2/
touch Es2/first.txt Es2/second.txt Es2/third.txt Es2/important.txt
git add Es2/first.txt Es2/third.txt Es2/important.txt
git commit -m "add first.txt third.txt and important.txt in Es2/"
echo 'new changes' > Es2/third.txt
echo 'Important changes!!' > Es2/important.txt
git add Es2/third.txt Es2/important.txt

echo -e "\nGit 4 dummies\nSecondo esercizio \n"
echo -e "Hai creato e committato un po' di file nella cartella Es2/ (dacci un occhio).\nInfatti, il tuo ultimo commit è:\n\n"
git log -1
echo -e "\n\nOra però vuoi eliminare tutti i file (anche dal versionamento) in Es2/ ESCLUSO Es2/important.txt, che dovrà essere solo rimosso dal versionamento..\nQuesti file non voglio vederli mai più, nemmeno nei prossimi commit. Dovresti pensare ad un modo per farlo."
echo -e "\nBuon lavoro :)"
echo -e "\nHint: prima di agire, guarda cosa c'è nella staging area!"
